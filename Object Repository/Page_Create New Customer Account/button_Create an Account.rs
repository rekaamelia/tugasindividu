<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Create an Account</name>
   <tag></tag>
   <elementGuidId>5110e313-3d88-4589-a931-781b00c00814</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.action.submit.primary > span</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='form-validate']/div/div/button/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>e4aa53f1-862d-4947-aafc-81749e9b4263</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Create an Account</value>
      <webElementGuid>32c64d71-3d1c-48f8-981e-c11ff2aff02c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;form-validate&quot;)/div[@class=&quot;actions-toolbar&quot;]/div[@class=&quot;primary&quot;]/button[@class=&quot;action submit primary&quot;]/span[1]</value>
      <webElementGuid>dd7a2bdf-2485-41d7-91da-7654e0e4e2f4</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='form-validate']/div/div/button/span</value>
      <webElementGuid>d1f33e2e-d0ad-458a-b145-c4f3251d2cc4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Confirm Password'])[1]/following::span[1]</value>
      <webElementGuid>8714835a-196f-4e91-b8bf-a877582aba7e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No Password'])[1]/following::span[2]</value>
      <webElementGuid>724ce162-1fe9-4d98-b37e-7bbda8ece7c7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Back'])[1]/preceding::span[1]</value>
      <webElementGuid>c17215cc-cf20-486a-8493-31fe0b185dc2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Newsletter'])[1]/preceding::span[2]</value>
      <webElementGuid>48af1af2-e102-4425-98b1-e5504da8fb16</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/div/div/button/span</value>
      <webElementGuid>57a8a182-671e-4cd6-b21f-3174d77e295f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Create an Account' or . = 'Create an Account')]</value>
      <webElementGuid>de0cd5db-cee0-4bb7-b0fe-7f23102bac1e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
