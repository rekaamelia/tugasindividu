<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>notif_register_success</name>
   <tag></tag>
   <elementGuidId>ebcbd631-48bf-484f-84af-bf5cda9eec0a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.message-success.success.message > div</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//main[@id='maincontent']/div/div[2]/div/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>8b8c3f49-c4a0-4ddb-a915-4530b6a3311f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-bind</name>
      <type>Main</type>
      <value>html: $parent.prepareMessageForHtml(message.text)</value>
      <webElementGuid>f4149a2c-c657-4c05-bb15-90f140b3e497</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Thank you for registering with Main Website Store.</value>
      <webElementGuid>fe0a7f7f-e965-48c7-b88a-1c74771c131e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;maincontent&quot;)/div[@class=&quot;page messages&quot;]/div[2]/div[@class=&quot;messages&quot;]/div[@class=&quot;message-success success message&quot;]/div[1]</value>
      <webElementGuid>b3cc0b82-a119-45c6-b2bf-65217a55e2a1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//main[@id='maincontent']/div/div[2]/div/div/div</value>
      <webElementGuid>d8e11ef4-83f3-48f2-814d-593af84ef74b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sign Out'])[4]/following::div[6]</value>
      <webElementGuid>bdd5bdbe-a007-44ac-bcdd-f383cdf46c63</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sign Out'])[3]/following::div[6]</value>
      <webElementGuid>267bb3ef-0eb6-400f-bf0c-6d15a7b4d568</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='My Account'])[4]/preceding::div[1]</value>
      <webElementGuid>7336e853-7873-4ba4-b277-76a618a83d02</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Account Information'])[1]/preceding::div[3]</value>
      <webElementGuid>9e1bb076-3976-41b4-b07b-ea8fafbff8d2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Thank you for registering with Main Website Store.']/parent::*</value>
      <webElementGuid>bfa534cb-7ecb-4676-84a3-db3c863b9fd3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[2]/div/div/div</value>
      <webElementGuid>b655fc5d-f7b8-4ad0-b998-51531e89eb07</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Thank you for registering with Main Website Store.' or . = 'Thank you for registering with Main Website Store.')]</value>
      <webElementGuid>9f737de6-ad77-48aa-a8c7-bfebfad53d33</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
