<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Error Login Notif</name>
   <tag></tag>
   <elementGuidId>1fce67a7-f5d3-4429-aa24-3cb42093c7ed</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.block.block-new-customer > div.block-title</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//main[@id='maincontent']/div[3]/div/div[2]/div[2]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>3c895254-ba2b-4eb5-a0d0-9f30ac63f0df</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>block-title</value>
      <webElementGuid>093adaa9-bc84-4ca3-8805-cd0e6489a6c5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
New Customers
</value>
      <webElementGuid>daceb9c7-6907-4c0b-b3e5-f663b2cf0396</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;maincontent&quot;)/div[@class=&quot;columns&quot;]/div[@class=&quot;column main&quot;]/div[@class=&quot;login-container&quot;]/div[@class=&quot;block block-new-customer&quot;]/div[@class=&quot;block-title&quot;]</value>
      <webElementGuid>d029f26d-ce4d-4fcb-8e45-3d39a3744845</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//main[@id='maincontent']/div[3]/div/div[2]/div[2]/div</value>
      <webElementGuid>fb2cb1cf-afd9-4b33-aee3-8a0c89a42bf1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Forgot Your Password?'])[1]/following::div[2]</value>
      <webElementGuid>cf76d4de-c61a-4459-bbec-c5be70669478</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sign In'])[3]/following::div[3]</value>
      <webElementGuid>72bb36f9-9c19-4c41-89be-4066cfc260c1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Create an Account'])[3]/preceding::div[1]</value>
      <webElementGuid>e77bd030-809e-4a9a-b682-973c04823cf0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[2]/div[2]/div</value>
      <webElementGuid>2feead2e-9301-41f6-9fad-fea8eba8fec5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
New Customers
' or . = '
New Customers
')]</value>
      <webElementGuid>fc4ca612-02ec-4ece-aa64-782c11d18337</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
