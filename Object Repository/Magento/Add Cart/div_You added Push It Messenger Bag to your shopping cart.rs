<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_You added Push It Messenger Bag to your shopping cart</name>
   <tag></tag>
   <elementGuidId>8be69d7b-7be7-44e4-92dd-e0bdcf48f769</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.message-success.success.message > div</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//main[@id='maincontent']/div/div[2]/div/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>03e91449-7051-4632-a8d3-9b61a2e22736</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-bind</name>
      <type>Main</type>
      <value>html: $parent.prepareMessageForHtml(message.text)</value>
      <webElementGuid>837fb104-8bea-4e02-b27f-ce7a0afcd72d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
You added Push It Messenger Bag to your shopping cart.</value>
      <webElementGuid>38ae4665-c29c-408a-a000-1a43a4daf9b7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;maincontent&quot;)/div[@class=&quot;page messages&quot;]/div[2]/div[@class=&quot;messages&quot;]/div[@class=&quot;message-success success message&quot;]/div[1]</value>
      <webElementGuid>e85ea3b1-54ab-47f6-9656-701ee413b092</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//main[@id='maincontent']/div/div[2]/div/div/div</value>
      <webElementGuid>9df2b015-b064-4f5a-80ff-023b98b81a6f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Push It Messenger Bag'])[3]/following::div[6]</value>
      <webElementGuid>18d77d4b-381d-48dd-94ce-faaa55ab7b95</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Home'])[1]/following::div[6]</value>
      <webElementGuid>80579a67-6b10-4007-84e8-807626d1e2f9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Push It Messenger Bag'])[4]/preceding::div[1]</value>
      <webElementGuid>16605db8-79fe-4178-a324-a856a47fe5f0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='You added Push It Messenger Bag to your']/parent::*</value>
      <webElementGuid>058b19b0-1664-4557-8ef6-4e0ef3fa0515</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//main/div/div[2]/div/div/div</value>
      <webElementGuid>b07d30b2-09e0-44d3-8896-500e50f8daa5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
You added Push It Messenger Bag to your shopping cart.' or . = '
You added Push It Messenger Bag to your shopping cart.')]</value>
      <webElementGuid>84c502be-fdca-41f0-9db7-e90c9af4c309</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
