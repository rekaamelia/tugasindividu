<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_My Cart11items</name>
   <tag></tag>
   <elementGuidId>6c4057b3-51ae-4b0f-92e1-43f4264423b7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a.action.showcart</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Toggle Nav'])[1]/following::a[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>391964e7-a8a7-430f-8be5-755541b8ed4d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>action showcart</value>
      <webElementGuid>ff2ae2ad-d175-4d5b-bfb2-8e9582665264</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>https://magento.softwaretestingboard.com/checkout/cart/</value>
      <webElementGuid>cedb478d-bb17-4c91-b760-f196e616bbbc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-bind</name>
      <type>Main</type>
      <value>scope: 'minicart_content'</value>
      <webElementGuid>787ec06f-9e6f-43f2-be5f-1976485411b2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
My Cart

1


1
items



</value>
      <webElementGuid>cc9d03fa-8be8-41cf-9780-71495ec4e7df</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;cms-home cms-index-index page-layout-1column&quot;]/div[@class=&quot;page-wrapper&quot;]/header[@class=&quot;page-header&quot;]/div[@class=&quot;header content&quot;]/div[@class=&quot;minicart-wrapper&quot;]/a[@class=&quot;action showcart&quot;]</value>
      <webElementGuid>9fa7f38a-97e6-4cca-9bfb-bdfa4b6c9cfe</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Toggle Nav'])[1]/following::a[2]</value>
      <webElementGuid>5b1edbce-77c6-464b-bb19-45365c3d8d17</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sign Out'])[2]/following::a[2]</value>
      <webElementGuid>ce547a39-4984-43b4-8c94-e132b5b2d9e8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'https://magento.softwaretestingboard.com/checkout/cart/')]</value>
      <webElementGuid>c7e807db-73da-4485-83e5-3a151173e1d7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/a</value>
      <webElementGuid>fa5f9014-b89a-4e7a-b24d-55de0dd674fb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'https://magento.softwaretestingboard.com/checkout/cart/' and (text() = '
My Cart

1


1
items



' or . = '
My Cart

1


1
items



')]</value>
      <webElementGuid>c3b2979a-9421-4faa-bdb6-24edc03919b6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
