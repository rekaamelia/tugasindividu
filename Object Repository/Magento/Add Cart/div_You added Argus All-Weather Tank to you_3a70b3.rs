<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_You added Argus All-Weather Tank to you_3a70b3</name>
   <tag></tag>
   <elementGuidId>d59971bd-8731-42fb-b234-3f8602ed1534</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.message-success.success.message > div</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//main[@id='maincontent']/div[2]/div[2]/div/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>2972e536-4cda-43e4-ab76-d1e386406896</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-bind</name>
      <type>Main</type>
      <value>html: $parent.prepareMessageForHtml(message.text)</value>
      <webElementGuid>94c75d68-4ac3-4bde-86a3-eb0c746653dd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
You added Argus All-Weather Tank to your shopping cart.</value>
      <webElementGuid>b76bd407-ee94-4d55-a3d0-c38103c63a35</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;maincontent&quot;)/div[@class=&quot;page messages&quot;]/div[2]/div[@class=&quot;messages&quot;]/div[@class=&quot;message-success success message&quot;]/div[1]</value>
      <webElementGuid>9af581da-72da-48d1-806d-5f3ae1fbf9e9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//main[@id='maincontent']/div[2]/div[2]/div/div/div</value>
      <webElementGuid>bc202450-f214-4e20-9432-e312d2e5e30b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Home Page'])[2]/following::div[6]</value>
      <webElementGuid>053d4d05-577e-4f98-b9ce-d197bf995e23</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sign Out'])[4]/following::div[7]</value>
      <webElementGuid>8ec67410-6c85-4044-95ff-32ea0828f9f4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='New Luma Yoga Collection'])[1]/preceding::div[2]</value>
      <webElementGuid>ddd2a261-33a3-43e0-938b-5d0ab33ce475</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='You added Argus All-Weather Tank to your']/parent::*</value>
      <webElementGuid>eeefcb49-1afe-41e5-854f-094a37dc6a56</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div/div/div</value>
      <webElementGuid>78160e02-6122-4293-9cb1-c24f78eb2b9b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
You added Argus All-Weather Tank to your shopping cart.' or . = '
You added Argus All-Weather Tank to your shopping cart.')]</value>
      <webElementGuid>0af93e5e-5a26-4dd6-83c6-6c33ff2eb52d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
