<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Add to Cart</name>
   <tag></tag>
   <elementGuidId>f17f4354-ecb0-49ea-a41b-2032683c81e6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#product-addtocart-button > span</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='product-addtocart-button']/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>ba3f5074-e61e-4f2e-ac6b-e48d8ff2335d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Add to Cart</value>
      <webElementGuid>ef2ea19c-b8c7-40f9-b49d-2d7a8b0e026f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;product-addtocart-button&quot;)/span[1]</value>
      <webElementGuid>da14c085-9184-403d-a5bc-eaa85783a2d4</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//button[@id='product-addtocart-button']/span</value>
      <webElementGuid>b643db36-c213-4fb5-99a3-f0f101ac87d7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Qty'])[1]/following::span[1]</value>
      <webElementGuid>30775a6b-6564-4e76-a537-cabebf630ae0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SKU'])[1]/following::span[2]</value>
      <webElementGuid>10ec9b96-0ada-44c8-97f3-239784e8d19d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add to Wish List'])[1]/preceding::span[1]</value>
      <webElementGuid>678b8c09-58c0-4d1c-9a1d-e9469996fb02</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add to Compare'])[1]/preceding::span[2]</value>
      <webElementGuid>e4c5d546-69d0-4b74-bf56-44a4685ac4c1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Add to Cart']/parent::*</value>
      <webElementGuid>c51580b5-0a7d-424d-9647-718c2d75a0b0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/div/div/div[2]/button/span</value>
      <webElementGuid>b9a67e50-83af-403c-96d6-b83306faf941</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Add to Cart' or . = 'Add to Cart')]</value>
      <webElementGuid>3fca7a88-731b-4186-b65c-9d3f75e8b663</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
