<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Confirm Password_password_confirmation</name>
   <tag></tag>
   <elementGuidId>b055fb45-9583-456f-a508-076536a4c19e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='password-confirmation']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@type = 'password' and @name = 'password_confirmation' and @title = 'Confirm Password' and @id = 'password-confirmation']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#password-confirmation</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>c24b929e-5473-4fde-95ea-eed7a14ce75c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>password</value>
      <webElementGuid>b38b00be-d6b8-429c-aee3-c479a965d209</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>password_confirmation</value>
      <webElementGuid>d5a396f3-5918-40a2-aab7-a8a7c34d3595</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Confirm Password</value>
      <webElementGuid>e363a021-ad0a-4b47-b2e1-c0ac4c897ed8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>password-confirmation</value>
      <webElementGuid>da6cd415-daf2-4936-bb89-90c7fd6b331e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>input-text</value>
      <webElementGuid>90ad2771-9619-45f5-a31a-704620fa1343</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-validate</name>
      <type>Main</type>
      <value>{required:true, equalTo:'#password'}</value>
      <webElementGuid>aa456bcd-82a7-4140-a14c-51667de36caa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>fd725b90-f946-4b4c-98d0-2b36856dc7c9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-required</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>d8cfa0df-7d49-4bcd-a534-d8495f4ba834</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;password-confirmation&quot;)</value>
      <webElementGuid>37ce55f4-2f33-4772-9a7b-1ee19f6fdc68</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='password-confirmation']</value>
      <webElementGuid>677f662a-1355-4833-b18a-cb3308743185</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='form-validate']/fieldset[2]/div[3]/div/input</value>
      <webElementGuid>c52e64c8-c83e-451f-be27-d38762da67c8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//fieldset[2]/div[3]/div/input</value>
      <webElementGuid>cb6f11a3-b60d-4a53-a354-a48adbc14615</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'password' and @name = 'password_confirmation' and @title = 'Confirm Password' and @id = 'password-confirmation']</value>
      <webElementGuid>f37840c4-0732-4bbc-96d8-ac2941445d3e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
