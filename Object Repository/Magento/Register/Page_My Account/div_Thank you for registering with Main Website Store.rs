<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Thank you for registering with Main Website Store</name>
   <tag></tag>
   <elementGuidId>0638aa08-3db5-44ea-9b9a-aca8ee291c3a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//main[@id='maincontent']/div/div[2]/div/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.message-success.success.message > div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>967ca8e6-7b35-410d-a2c8-2fcca555ea52</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-bind</name>
      <type>Main</type>
      <value>html: $parent.prepareMessageForHtml(message.text)</value>
      <webElementGuid>67d37d2d-2d78-46d9-8d78-7da697f949e9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Thank you for registering with Main Website Store.</value>
      <webElementGuid>80b4a80c-0634-4e7e-ae84-dd0eceac6570</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;maincontent&quot;)/div[@class=&quot;page messages&quot;]/div[2]/div[@class=&quot;messages&quot;]/div[@class=&quot;message-success success message&quot;]/div[1]</value>
      <webElementGuid>fb9a701e-dc1b-4f2d-833c-096170bb45e3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//main[@id='maincontent']/div/div[2]/div/div/div</value>
      <webElementGuid>d0ec5aa7-3bfd-4165-9671-5a6fb80b7389</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sign Out'])[4]/following::div[6]</value>
      <webElementGuid>c5a66513-9d01-4564-8fc9-6762883f8456</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sign Out'])[3]/following::div[6]</value>
      <webElementGuid>f3ee8cba-02ae-4aa5-8c76-e6ce1b02af50</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='My Account'])[4]/preceding::div[1]</value>
      <webElementGuid>a7c352a0-7bcf-48ef-826e-2dccf8b51bac</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Account Information'])[1]/preceding::div[3]</value>
      <webElementGuid>a8cc61f1-1c9a-4cfe-ba3e-53aaf07a457b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Thank you for registering with Main Website Store.']/parent::*</value>
      <webElementGuid>b92bf0ba-404a-4479-b42c-16b145fcc607</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[2]/div/div/div</value>
      <webElementGuid>92b75286-210d-4420-8f2b-5f8965a32624</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Thank you for registering with Main Website Store.' or . = 'Thank you for registering with Main Website Store.')]</value>
      <webElementGuid>9f8a7fd1-2a2d-4496-b4ab-f8f9ddfc74d3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
