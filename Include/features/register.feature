#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template

@Register
Feature: Register Functionality
  User want create new account

  @Test
  Scenario Outline: User want to login with <condition>
    Given User go to Register page 
    When User input first name <first_name> 
    And User input last name <last_name>
    And User input email with <email> 
    And User input password <password>
    And User input confirm password with <com_password>
    And User click create an account
    Then User verify the <status> register

    Examples: 
      | case     | condition				 				| first_name  | last_name | email 					 | password  | com_password | status 		 			|						
      | positive | valid credentials				| correct			| correct		| correct					 | correct	 | correct		  | sucessfull 			|
      | negative | invalid format email 		| correct	    | correct		| incorrect format | correct   | correct      | failed email		|      
      | negative | invalid confirm password |	correct			| correct		| correct					 | correct   | incorrect		|	failed password	|
      
      
      