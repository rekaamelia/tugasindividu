#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template

@Login
Feature: Login Functionality
	As a user In order to access my account I want to be able to login

  @test
  Scenario Outline: Successful Login
    Given I am on the login page
    When I fill email with <email>
    And I fill password with <pass>
    When I click login button
    Then I should be redirected to the home page
    And I close the browser

    Examples: 
      | email								| pass 											| 
      | prasdavy@gmail.com 	|	bbfz/EFVRyW36CwJkhajVA== 	|
      
  @test
  Scenario Outline: Failed Login
    Given I am on the login page
    When I fill email with <email>
    And I fill wrong password <pass>
    When I click login button
    Then I should see error message
    And I close the browser

    Examples: 
      | email								| pass 	| 
      | prasdavy@gmail.com 	|	kekek |