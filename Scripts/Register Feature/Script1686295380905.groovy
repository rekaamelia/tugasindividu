import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import ch.qos.logback.core.joran.conditional.ElseAction
import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class register {

	@Given("User go to Register page")
	public void user_go_to_Register_page() {
		WebUI.openBrowser('')
		WebUI.navigateToUrl('magento.softwaretestingboard.com/customer/account/create/')
		WebUI.maximizeWindow()
	}

	@When("User input first name correct")
	public void user_input_first_name_correct() {
		WebUI.setText(findTestObject('Object Repository/Page_Create New Customer Account/input_First Name_firstname'), 'reka')
	}

	@When("User input last name correct")
	public void user_input_last_name_correct() {
		WebUI.setText(findTestObject('Object Repository/Page_Create New Customer Account/input_Last Name_lastname'), 'amelia')
	}

	String Email = CustomKeywords.'randomEmail.getEmail'('rekaameliaa', 'gmail.com')

	@When("User input email with (.*)")
	public void user_input_email_with_email(String email) {
		if(email=='correct') {
			WebUI.setText(findTestObject('Object Repository/Page_Create New Customer Account/input_Email'), Email)
		}
		else if (email=='incorrect format') {
			WebUI.setText(findTestObject('Object Repository/Page_Create New Customer Account/input_Email'), 'rekaameliaa14280gmail.com')
		}
	}

	@When("User input password correct")
	public void user_input_password_correct() {
		WebUI.setText(findTestObject('Object Repository/Page_Create New Customer Account/input_Password'), 'Buah@123')
	}

	@When("User input confirm password with (.*)")
	public void user_input_cofirm_password_with_com_password(String com_password) {
		if(com_password=='correct') {
			WebUI.setText(findTestObject('Object Repository/Magento/Register/Page_Create New Customer Account/input_Confirm Password_password_confirmation'), 'Buah@123')
		}
		else if (com_password=='incorrect'){
			WebUI.setText(findTestObject('Object Repository/Magento/Register/Page_Create New Customer Account/input_Confirm Password_password_confirmation'), 'Buah123')
		}
	}

	@When("User click create an account")
	public void user_click_create_an_account() {
		WebUI.click(findTestObject('Object Repository/Magento/Register/Page_Create New Customer Account/Page_Create New Customer Account/button_Create an Account'))
	}

	@Then("User verify the (.*) register")
	public void user_verify_the_status_register(String status) {
		if(status=='sucessfull') {
			WebUI.verifyElementPresent(findTestObject('Object Repository/Magento/Register/Page_My Account/span_My Account'), 0)
		}
		else if (status=='failed email'){
			WebUI.verifyElementPresent(findTestObject('Object Repository/Magento/Register/Page_Create New Customer Account/div_Please enter a valid email address (Ex johndoedomain.com)'), 0)
		}
		else if (status=='failed password'){
			WebUI.verifyElementPresent(findTestObject('Object Repository/Magento/Register/div_Please enter the same value again'), 0)
		}
	}
}